<?php

namespace App\Http\Import;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportSpreadsheet
{
    public function importData(Request $request) {
        $clearArray = [];
        $clearData= [];
        $returnData =[];

        $initialRead = IOFactory::load($request->file);
        $initialData = $initialRead->getActiveSheet()->toArray();

        foreach($initialData as $data) {
            $arrayData = [];
            foreach ($data as $item) {
                if ($item) {
                    array_push($arrayData, $item);
                }
            }
            array_push($clearArray, $arrayData);
        }

        $filterArray = array_filter($clearArray, function($data) {
            return sizeof($data) > 0;
        });

        foreach ($filterArray as $data) {
            array_push($clearData, $data);
        }

        for ($i=1; $i < count($clearData); $i++) {
            array_push($returnData, $clearData[$i]);
        }

        return $returnData;
    }
}
