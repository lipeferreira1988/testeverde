<?php

namespace App\Http\Controllers;

use App\Models\Waste;
use App\Http\Requests\SpreadsheetRequest;
use App\Http\Import\ImportSpreadsheet;
use App\Http\Requests\WasteRequest;

class WasteController extends Controller
{
    private $waste;
    private $importSpreadsheet;

    public function __construct(Waste $waste, ImportSpreadsheet $importSpreadsheet) {
        $this->waste = $waste;
        $this->importSpreadsheet = $importSpreadsheet;
    }

    public function index()
    {
        $wastes = $this->waste->all();

        return response()->json($wastes, 200);
    }

    public function store(SpreadsheetRequest $request)
    {
        $data = $this->importSpreadsheet->importData($request);
        try {
            foreach ($data as $item) {
                $this->waste->create([
                    'waste_common_name' => $item[0],
                    'waste_type' => $item[1],
                    'category' => $item[2],
                    'treatment_technology' => $item[3],
                    'class' => $item[4],
                    'unity_of_measurement' => $item[5],
                    'weight' => $item[6]
                ]);
            }
            return response()->json(['msg' => 'planílha processada com sucesso!'], 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function show($id)
    {
        try {
            $waste = $this->waste->findOrFail($id);
            return response()->json($waste, 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function update(WasteRequest $request, $id)
    {
        $data = $request->all();
        try {
            $waste = $this->waste->findOrFail($id);
            $waste->update($data);
            return response()->json(['msg' => 'Resíduo alteradocom sucesso!'], 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function destroy($id)
    {
        try {
            $waste = $this->waste->findOrFail($id);
            $waste->delete();
            return response()->json(['msg' => 'Resíduo deletado com sucesso!'], 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
