<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WasteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'waste_common_name' => 'required',
            'waste_type' => 'required',
            'category' => 'required',
            'treatment_technology' => 'required',
            'class' => 'required',
            'unity_of_measurement' => 'required',
            'weight' => 'required'
        ];
    }
}
