<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Waste extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'waste_common_name',
        'waste_type',
        'category',
        'treatment_technology',
        'class',
        'unity_of_measurement',
        'weight'
    ];
}
