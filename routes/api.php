<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WasteController;

Route::get('/wastes', [WasteController::class, 'index']);
Route::post('/wastes', [WasteController::class, 'store']);
Route::get('/wastes/{id}', [WasteController::class, 'show']);
Route::put('/wastes/{id}', [WasteController::class, 'update']);
Route::delete('/wastes/{id}', [WasteController::class, 'destroy']);
